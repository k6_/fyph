/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/
#ifndef _TEST_HPP_
# define _TEST_HPP_

# define BOOST_TEST_DYN_LINK
# define BOOST_TEST_MODULE fyph

# include <boost/test/unit_test.hpp>

#endif
