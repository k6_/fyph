/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/
#include <test.hpp>
#include <Mod.hpp>

BOOST_AUTO_TEST_SUITE(Mod)

fyph::Logger *logger = new fyph::Logger();
fyph::ModLoader *loader = new fyph::ModLoader(logger);

BOOST_AUTO_TEST_CASE(ModLoad)
{
  BOOST_CHECK_THROW(loader->load(""), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("asdasd"), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("/etc/shadow"), fyph::ModLoader::LoadError);
  BOOST_CHECK_NO_THROW(loader->load("../mods/GCipher.so"));
  BOOST_CHECK_THROW(loader->load("../mods/GCipher.so"), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("./dat/null_init.so"), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("./dat/miss_dep.so"), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("./utest"), fyph::ModLoader::LoadError);
  BOOST_CHECK_THROW(loader->load("/usr/lib64/libbfd.so"), fyph::ModLoader::LoadError);
}

BOOST_AUTO_TEST_CASE(ModDirLoad)
{
  BOOST_CHECK_THROW(loader->loadDir("./asd"), fyph::ModLoader::DirError);
  BOOST_CHECK_NO_THROW(loader->loadDir("../src/"));
  BOOST_CHECK_NO_THROW(loader->loadDir("./mods/"));
}

BOOST_AUTO_TEST_CASE(ModUnload)
{
  BOOST_CHECK_THROW(loader->unload("asdasd"), fyph::ModLoader::UnloadError);
  BOOST_CHECK_NO_THROW(loader->unload("1.so"));
}

BOOST_AUTO_TEST_SUITE_END()
