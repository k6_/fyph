#---+                                FYPH                                 +---#
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
#--+                                                                       +--#


NAME    =  fyph
SRCS    =  main.cpp \
           misc.cpp \
           Fyph.cpp \
					 NLSck.cpp \
           NFQ.cpp \
           Mod.cpp \
           Pkt.cpp \
           Error.cpp \
					 Logger.cpp
OBJS    =  $(addprefix obj/, $(notdir $(SRCS:.cpp=.o)))
INCS    =  -I include

CXXFLAGS  := -Wall -Wextra -pedantic -std=c++14 `libgcrypt-config --cflags` -ggdb
LDFLAGS   := -l netfilter_queue \
             `libgcrypt-config --libs` \
             -l pthread \
             -l dl -rdynamic \
						 -l mnl
CC        := g++

all: obj/ $(NAME)

obj/:
	mkdir $@

$(NAME): $(OBJS)
	$(CC) -o $@ $(OBJS) $(LDFLAGS)

obj/%.o: src/%.cpp
	$(CC) -c $< -o $@ $(CXXFLAGS) $(INCS)

clean:
	rm -rf obj/

fclean: clean
	rm -f $(NAME)

re: fclean all

test: $(OBJS)
	cd unit_test/ && make

.PHONY: all clean fclean re test
