/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _NFQ_H_
# define _NFQ_H_

# include <errno.h>
# include <error.h>
# include <string.h>
# include <arpa/inet.h>
# include <libnetfilter_queue/libnetfilter_queue.h>
# include <linux/netfilter.h>
# include <sys/uio.h>

# include <iostream>
# include <exception>
# include <mutex>

# include <Mod.hpp>
# include <Pkt.hpp>
# include <NLSck.hpp>

namespace                 fyph
{

  struct                  NFQ : NLSck
  {
    public:

      struct              Error : public fyph::Error
      {
        public:
          Error(const std::string &msg, int queue = -1, fyph::Logger *logger = NULL);
          Error(const char *msg, int queue = -1, fyph::Logger *logger = NULL);
      };

      NFQ(int queue = 0, Logger *log = NULL);
      ~NFQ(void);
      void                setOptions(bool (*live)(void), unsigned timeout = 200);
      static void         intercept(NFQ *inst, NFQ::Error *err, void *data = NULL,
                                     fyph::Logger *logger = NULL);

      int                 getFd(void) const { return (_fd); }

    private:
      int                 _queue;
      struct nfq_handle   *_handle = NULL;
      struct nfq_q_handle *_qHandle = NULL;
      bool                (*_live)(void) = NULL;

      static void         setVerdict(struct mnl_socket *hdl, uint32_t queue,
                                      uint32_t id, uint32_t verdict,
                                      struct iovec *pdata,
                                      Logger *log = NULL);
      static struct nlmsghdr *setHdr(uint8_t *buf, int type, uint32_t queue);
      ssize_t             recvPkt(t_iovec *iov);
      static int          callback(const struct nlmsghdr *nlhdr, void *data);
      static t_iovec      *getPkt(struct nfq_data *nfa, int hook);
      static void         treatPkt(int queue, struct nlattr **attr,
                                    struct nfqnl_msg_packet_hdr *ph,
                                    ModLoader *loader);
      };

}

#endif
