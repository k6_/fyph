/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _GCIPHER_HPP_
# define _GCIPHER_HPP_

# include <unistd.h>
# include <errno.h>
# include <error.h>
# include <string.h>
# include <gcrypt.h>

# include <exception>

# include <Mod.hpp>

namespace              fyph
{

  struct               GCipher : Mod
  {
    public:
      struct             Cipher
      {
        public:
          Cipher(int cypher, int mode = 0);
          ~Cipher(void);
          void           encrypt(t_iovec *clrVec, t_iovec *encVec);
          void           decrypt(t_iovec *clrVec, t_iovec *encVec);
          t_iovec        *align(t_iovec *v, size_t blksz, bool always=false);

          int            getCipher(void) const { return (_cipher); }
          int            getMode(void) const { return (_mode); }
          void           setKey(const t_iovec *key);
          const t_iovec  *getKey(void) const { return (_key); }
		  fyph::Verdict  treatPkt(fyph::ModData *mData, fyph::Pkt *pkt, int hk);
          void           eCall(t_iovec *pdata);
          void           dCall(t_iovec *pdata);

        private:
          gcry_cipher_hd_t   _handle;
          const int      _cipher;
          const int      _mode;
          const t_iovec  *_key;

      };

      t_callback         callback;
      void               del(void);
  };

}

extern "C" void          *init(void);

#endif
