/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/
#ifndef _LOG_HPP_
# define _LOG_HPP_

# include <syslog.h>
# include <stdarg.h>

# include <string>
# include <ostream>

namespace            fyph
{

  struct             Logger
  {
    public:
      Logger(void);
      ~Logger(void);
      template<typename... Args>
      void           debug(const std::string &src, const std::string &fmt,
                           const Args&... args);
      template<typename... Args>
      void           info(const std::string &src, const std::string &fmt,
                          const Args&... args);
      template<typename... Args>
      void           notice(const std::string &src, const std::string &fmt,
                            const Args&... args);
      template<typename... Args>
      void           warning(const std::string &src, const std::string &fmt,
                             const Args&... args);
      template<typename... Args>
      void           error(const std::string &src, const std::string &fmt,
                           const Args&... args);

    private:
      template<int prio, typename... Args>
      void           log(const std::string &fmt, Args&... args);
  };

}


template<int prio, typename... Args>
void           fyph::Logger::log(const std::string &fmt, Args&... args)
{
  syslog(prio, fmt.c_str(), std::forward<Args>(args)...);
}

# define LOGF(x, y)   \
  template<typename... Args> void         x(const std::string &src, \
                                            const std::string &fmt, \
                                            const Args&... args) \
{ \
  log<y>(src + ": " + fmt, std::forward<const Args&>(args)...); \
}

LOGF(fyph::Logger::debug, LOG_DEBUG)
LOGF(fyph::Logger::info, LOG_INFO)
LOGF(fyph::Logger::notice, LOG_NOTICE)
LOGF(fyph::Logger::warning, LOG_WARNING)
LOGF(fyph::Logger::error, LOG_ERR)

#endif
