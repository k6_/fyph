/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/
#ifndef __NLSCK_HPP__
# define __NLSCK_HPP__

extern "C"
{
#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <linux/types.h>
}

# include <Logger.hpp>

namespace               fyph
{
  struct                NLSck
  {
    public:
      NLSck(int type,   fyph::Logger *log = NULL);
      uint16_t          getPort(void) { return (_port); }
      struct mnl_socket *getSck(void) { return (_hdl); }

    protected:
      Logger            *_logger;
      mnl_socket        *_hdl;
      uint16_t          _bufsz;
      int               _fd;
      int               _port;
  };
}

#endif
