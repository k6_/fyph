/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _FYPH_H_
# define _FYPH_H_

# include <unistd.h>
# include <arpa/inet.h>

# include <exception>
# include <iostream>
# include <thread>
# include <atomic>
# include <map>

# include <NFQ.hpp>
# include <GCipher.hpp>

namespace             fyph
{
  struct              Fyph
  {
    public:
      Fyph(struct iovec *iov);
      ~Fyph(void);
      void            addMod(const std::string &mod);
      void            addModDir(const std::string &modDir);
      void            addQueue(int queue);
      void            rmQueue(int queue);
      void            run();
      static bool     live(void);
      static void     kill(void);

      ModLoader       *_loader;
	    Logger          *_logger;

    private:
      void            watchQueue(int queue);

	    std::mutex      *_mtx;
      std::map<int, std::thread *> _thread;
      std::map<int, fyph::NFQ *> _queue;
  };
}

# endif
