/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _PKT_H_
# define _PKT_H_

# include <string.h>
# include <net/ethernet.h>
# include <netinet/ip.h>
# include <netinet/tcp.h>

# include <array>
# include <map>
# include <algorithm>

# include <misc.hpp>
# include <Error.hpp>

namespace              fyph
{
  enum                 LayerID
  {
    LINK = 1,
    NETWORK,
    TRANSPORT,
    SESSION,
    PRESENTATION,
    APPLICATION,
    NLAYER
  };

  struct               IProto
  {
    public:
      virtual ~IProto() {  };
      virtual size_t   getTLen() const = 0;
      virtual uint32_t getID(void) const = 0;
      virtual uint32_t getNextID(void) const = 0;
      virtual LayerID  getLayerID(void) const = 0;
      virtual void     mangle(t_iovec *vec, size_t tlen) = 0;
      virtual bool     verify(void) = 0;
  };

  struct               IP : IProto
  {
    public:
      IP(t_iovec *v);
      virtual ~IP() {  }
      uint32_t         getID(void) const { return (_id); }
      uint32_t         getNextID(void) const { return (_hdr->protocol); }
      LayerID          getLayerID(void) const { return (_layerID); }
      size_t           getTLen(void) const { return (_hdr->ihl * 4); }
      void             mangle(t_iovec *vec, size_t tlen);
      bool             verify(void);

      static uint16_t  checksum(t_iovec *v);

    private:
      iphdr            *_hdr;

    public:
      static const uint32_t   _id = ETHERTYPE_IP;
      static const LayerID    _layerID = LayerID::NETWORK;
  };

  struct               TCP : IProto
  {
    public:
      TCP(t_iovec *v);
      virtual ~TCP();
      uint32_t         getID(void) const { return (_id); }
      uint32_t         getNextID(void) const { return (-1); }
      LayerID          getLayerID(void) const { return (_layerID); }
      size_t           getTLen(void) const
                         { return (_hdr->doff * 4 + _data->iov_len); }
      t_iovec          *getData(void) const { return (_data); }
      void             mangle(t_iovec *v, size_t tlen);
      bool             verify(void);

    private:
      tcphdr           *_hdr;
      t_iovec          *_data;

      void             updateTotLen(void);

    public:
      static const LayerID    _layerID = LayerID::TRANSPORT;
      static const uint32_t   _id = IPPROTO_TCP;
  };

  template<typename T> IProto *newLayer(struct iovec *v) {
    return (new T(v));
  }

  struct               Layer
  {
    public:
      typedef          IProto *(*t_newLayer)(t_iovec *v);
      typedef          std::map<uint32_t, t_newLayer> mtype;


      static mtype     newLayer;
  };

  struct               Pkt
  {
    public:
      Verdict          verdict;
      std::array<IProto *, LayerID::NLAYER> _layer;

      Pkt(int id, uint32_t proto, t_iovec *v);
      ~Pkt(void);
      template<typename T> T *layer(void);
      void            mangle(void);

      t_iovec         *get(void) { return (&_pkt); };

    private:
      t_iovec         _pkt;
      int             _id;

      void            updateTLen();
      void            nextLayer(uint32_t &proto, LayerID &layer, size_t &off,
                                t_iovec *lvec);

    public:
      struct          Error : public fyph::Error
      {
        Error(const int id, const std::string &msg)
          : fyph::Error("Pkt", "packet %d: %s", id, msg.c_str()) {  }
      };

      struct          TrunkedError : public Pkt::Error
      {
        TrunkedError(const int id)
          : Pkt::Error(id, "too short - looks trunked") {  }
      };

      struct          IllFormedError : public Pkt::Error
      {
        IllFormedError(const int id) : Pkt::Error(id, "ill formed") {  }
      };

      struct          ProtoError : public Pkt::Error
      {
        ProtoError(const int id, const uint32_t proto, const int layerId)
          : Pkt::Error(id, "unimplemented protocol id " + std::to_string(proto)
                       + " for layer " + std::to_string(layerId))
        {  }
      };
  };

  template<typename T> T  *fyph::Pkt::layer(void)
  {
    std::array<fyph::IProto *, fyph::LayerID::NLAYER>::const_iterator it;
    if ((it = std::find_if(_layer.begin(), _layer.end(),
        [](IProto *p) {
              return (p ? T::_id == p->getID() : false);
           })
        ) != _layer.end())
      return ((T *)(*it));
    return (NULL);
  }

}

#endif
