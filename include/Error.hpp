/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _ERROR_H_
# define _ERROR_H_

# include <string.h>
# include <errno.h>

# include <memory>
# include <string>
# include <exception>
# include <cstdio>

# include <Logger.hpp>

namespace                fyph
{

  struct                 Error : public std::exception
  {
    public:
      template<typename ... Args>
      Error(const std::string &src, fyph::Logger *l, const std::string &fmt,
            const Args&... args);
      template<typename ... Args>
      Error(const std::string &src, const std::string &fmt, const Args&... args);
      template<typename ... Args>
      std::string        fmtStr(const std::string &fmt, Args ... args);
      virtual ~Error() throw() {  }
      virtual const char *what() const throw () { return (_msg.c_str()); };
	    static std::string getMsg(const std::string &msg);

    protected:
      std::string        _msg;
      int                _errno;
  };

}

template<typename ... Args>
fyph::Error::Error(const std::string &src, fyph::Logger *log,
                   const std::string &fmt, const Args&... args)
  : _msg(fmtStr(fmt, std::forward<const Args&>(args)...) + (errno ? std::string(": ") + strerror(errno) : ""))
{
  if (log)
    log->error(src, "%s", _msg.c_str());
}

template<typename ... Args>
fyph::Error::Error(const std::string &src, const std::string &fmt, const Args&... args)
  : _msg(src + ": " + fmtStr(fmt, std::forward<const Args&>(args)...))
{
}

template<typename ... Args>
std::string fyph::Error::fmtStr(const std::string &fmt, Args ... args)
{
  size_t size = std::snprintf(nullptr, 0, fmt.c_str(), args ...) + 1;
  std::unique_ptr<char[]> buf(new char[size]);
  std::snprintf(buf.get(), size, fmt.c_str(), args ...);
  return (std::string(buf.get(), buf.get() + size - 1));
}

#endif
