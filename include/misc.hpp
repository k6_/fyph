/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _MISC_HPP_
# define _MISC_HPP_

# include <sys/uio.h>
# include <ctype.h>

# include <iostream>
# include <iomanip>

# define UN              __attribute__((unused))

namespace                fyph
{

  enum                   Verdict
  {
    ACCEPT = 0,
    DROP
  };

}



typedef struct iovec     t_iovec;

namespace                fyph
{
  void                   dumpHex(std::ostream &out, const uint8_t *dat,
                                 size_t datsz);
   void                  dumpAscii(std::ostream &out, const uint8_t *dat,
                                   size_t datsz);
}

static UN std::ostream   &operator<<(std::ostream &out, const t_iovec *vec)
{
  size_t                 i;
  size_t                 lineSz;

  for (i = 0; i < vec->iov_len; i += 16)
  {
    lineSz = (vec->iov_len - i) / 16 ? 16 : vec->iov_len % 16;
    fyph::dumpHex(out, (uint8_t *)vec->iov_base + i, lineSz);
    out << "  | ";
    fyph::dumpAscii(out, (uint8_t *)vec->iov_base + i, lineSz);
    out << std::endl;
  }
  return (out);
}

#endif
