/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#ifndef _MOD_H_
# define _MOD_H_

# include <misc.hpp>
# include <net/ethernet.h>
# include <netinet/ip.h>
# include <netinet/tcp.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <dirent.h>
# include <dlfcn.h>
# include <error.h>

# include <mutex>
# include <map>
# include <utility>
# include <algorithm>

# include <Logger.hpp>
# include <Error.hpp>
# include <Pkt.hpp>
# include <misc.hpp>

namespace                 fyph
{
  struct                  NFQ;

  struct                  Mod
  {
    public:
      typedef fyph::Verdict t_callback(void *data, void *pkt, int hook,
                                       const uint8_t hw_addr[8]);
      virtual t_callback  callback = 0;
      virtual void        del(void) = 0;
  };

  struct                  ModData
  {
    public:
      ModData(struct iovec *k, fyph::Logger &logger,
              std::map<int, fyph::NFQ *> &queue)
          : key(k), mtx(new std::mutex()), logger(logger), queue(queue)
      { }
      struct iovec        *key;
      std::mutex          *mtx;
      fyph::Logger        &logger;
      std::map<int, fyph::NFQ *> &queue;
  };

  struct                  ModLoader
  {

    public:
      virtual ~ModLoader(void);
      ModLoader(fyph::Logger *logger)
        : _mod({}), _handle({}), _logger(logger) {  }

      void                load(const std::string &pnam);
      void                unload(const std::string &fname);
      void                loadDir(const std::string &modDir);
      fyph::Verdict       callMods(t_iovec *pdata, uint32_t hwproto, int hook,
                                   const uint8_t hw[8]);
      size_t              modn(void) { return (_mod.size()); }
      static
      std::string         dlError(void);

      ModData             *data;

    private:
      fyph::Verdict       returnPkt(Pkt *pkt, t_iovec *pdata);

      std::map<std::string, Mod *>  _mod;
      std::map<std::string, void *> _handle;
      fyph::Logger        *_logger;

      void                init(const std::string &fname);

    public:
      struct              Error :  public fyph::Error
      {
        public:
          virtual ~Error() throw() {  };
          Error(const std::string &act, const std::string &msg,
                fyph::Logger *logger = NULL)
            : fyph::Error("ModLoader", logger, msg != "" ? "%s: %s" : "%s", act.c_str(),
                          msg.c_str()) {  }
          static std::string getMsg(const std::string &act,
                                    const std::string &msg);
      };

      struct              LoadError : public ModLoader::Error
      {
        public:
          LoadError(const std::string &mod, const std::string &msg,
                             fyph::Logger *logger = NULL)
            : ModLoader::Error("loading module \'" + mod + "\'", msg, logger)
          {  }
      };

      struct              UnloadError : public ModLoader::Error
      {
        public:
          UnloadError(const std::string &mod, const std::string &msg,
                               fyph::Logger *logger = NULL)
            : ModLoader::Error("unloading module \'" + mod + "\'", msg, logger)
          {  }
      };

      struct              DirError : public ModLoader::Error
      {
        public:
          DirError(const std::string &dir, const std::string &msg,
                              fyph::Logger *logger = NULL)
            : ModLoader::Error("opening directory \'" + dir + "\'", msg, logger)
          {  }
      };
   };

}

#endif
