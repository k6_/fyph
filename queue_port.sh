#!/bin/sh

if [ $# -ne 1 ]; then
	echo "usage: " $0 " port"
	exit 1
fi

iptables -A INPUT -j NFQUEUE --queue-num 0 -p tcp --destination-port $1
iptables -A INPUT -j NFQUEUE --queue-num 0 -p tcp --source-port $1
iptables -A OUTPUT -j NFQUEUE --queue-num 1 -p tcp --destination-port $1
iptables -A OUTPUT -j NFQUEUE --queue-num 1 -p tcp --source-port $1
exit 0
