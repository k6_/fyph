#include <Logger.hpp>

fyph::Logger::Logger(void)
{
  openlog("fyph", LOG_PID | LOG_PERROR, LOG_USER);
}

fyph::Logger::~Logger(void)
{
  closelog();
}

