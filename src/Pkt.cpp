/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <Pkt.hpp>

fyph::Layer::mtype     fyph::Layer::newLayer = {
  {ETHERTYPE_IP, &fyph::newLayer<fyph::IP>},
  {IPPROTO_TCP, &fyph::newLayer<fyph::TCP>}
};


fyph::IP::IP(struct iovec *v)
{
  if (v->iov_len < sizeof(*_hdr) || !v->iov_base)
    throw (fyph::Pkt::TrunkedError(_id));
  _hdr = (iphdr *)v->iov_base;
  if (!verify() || v->iov_len > IP_MAXPACKET)
    throw (fyph::Pkt::IllFormedError(_id));
}

bool           fyph::IP::verify(void)
{
  bool         sane;

  sane = _hdr->version == IPVERSION;
  sane &= _hdr->tot_len >= sizeof(_hdr);
  return (sane);
}

uint16_t       fyph::IP::checksum(t_iovec *v)
{
  uint64_t     sum = 0;

  for (size_t i = 0; i < v->iov_len; i+=sizeof(uint16_t))
  {
    sum += *(uint16_t *)((uint64_t)v->iov_base + i);
    while (sum & 0xffff0000)
      sum = (sum & 0xffff) + (sum >> 16);
  }
  return (~sum);
}

void            fyph::IP::mangle(struct iovec *v, size_t tlen)
{
  if (v->iov_len < sizeof(*_hdr) || !v->iov_base)
    throw (fyph::Pkt::TrunkedError(_id));
  _hdr->check = 0;
  _hdr->tot_len = htons(tlen);
  t_iovec hdr = {_hdr, sizeof(*_hdr)};
  _hdr->check = checksum(&hdr);
  memcpy(v->iov_base, _hdr, sizeof(*_hdr));
}

fyph::TCP::TCP(struct iovec *v)
{
  if (v->iov_len < sizeof(*_hdr))
    throw (fyph::Pkt::TrunkedError(_id));
  if (!(_data = (t_iovec *)malloc(sizeof(*_data))))
    throw std::runtime_error("no more memory available");
  _hdr = (tcphdr *)v->iov_base;
  _data->iov_base = (void *)((uint8_t *)_hdr + _hdr->doff * 4);
  _data->iov_len = v->iov_len - _hdr->doff * 4;
}

fyph::TCP::~TCP(void)
{
  if ((uint64_t)_data != (uint64_t)_hdr + _hdr->doff * 4)
    free(_data);
}

void            fyph::TCP::mangle(struct iovec *v, size_t)
{
  if (v->iov_len < getTLen() || !v->iov_base)
    throw (fyph::Pkt::TrunkedError(_id));
  memcpy(v->iov_base, _hdr, _hdr->doff * 4);
  if (_data)
    memcpy((void *)((uint64_t)v->iov_base + _hdr->doff * 4), _data->iov_base,
        _data->iov_len);
}

bool            fyph::TCP::verify(void)
{
  return (true);
}

fyph::Pkt::Pkt(int id, uint32_t proto, struct iovec *vec)
	: verdict(fyph::Verdict::DROP), _id(id)
{
  fyph::LayerID        layer = fyph::LayerID::LINK;
  size_t               off = 0;
  t_iovec              lvec;

  std::fill(_layer.begin(), _layer.begin() + fyph::LayerID::NLAYER,
             (IProto *)NULL);
  while (layer < fyph::LayerID::NLAYER && off < vec->iov_len)
  {
    lvec.iov_base = (void *)((uint64_t)vec->iov_base + off);
    lvec.iov_len = vec->iov_len - off;
    nextLayer(proto, layer, off, &lvec);
  }
}

fyph::Pkt::~Pkt(void)
{
  for (auto it = _layer.begin(); it != _layer.end(); it++)
    delete (*it);
}

void             fyph::Pkt::nextLayer(uint32_t &proto, fyph::LayerID &layer,
                                      size_t &off, t_iovec *lvec)
{
  if (fyph::Layer::newLayer.find(proto) != fyph::Layer::newLayer.end())
   {
     if ((_layer[layer] = fyph::Layer::newLayer[proto](lvec)))
     {
       off += _layer[layer]->getTLen();
       proto = _layer[layer]->getNextID();
       layer = (fyph::LayerID)((unsigned)layer + 1);
     }
   }
  else
    throw (fyph::Pkt::ProtoError(_id, proto, layer));
}


void              fyph::Pkt::updateTLen(void)
{
  _pkt.iov_len = 0;
  for (auto it = _layer.begin(); it != _layer.end(); it++)
  {
    if (*it)
      _pkt.iov_len += (*it)->getTLen();
  }
}

void               fyph::Pkt::mangle(void)
{
  t_iovec          ppkt;

  updateTLen();
  if (!(_pkt.iov_base = malloc(_pkt.iov_len)))
    throw (std::runtime_error("memory exhausted"));
  ppkt = _pkt;
  for (auto it = _layer.begin(); it != _layer.end(); it++)
  {
    if (*it)
    {
      (*it)->mangle(&ppkt, _pkt.iov_len);
      ppkt.iov_base = (void *)((uint64_t)ppkt.iov_base + (*it)->getTLen());
      ppkt.iov_len -= (*it)->getTLen();
    }
  }
}
