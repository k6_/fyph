/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <Mod.hpp>

fyph::ModLoader::~ModLoader(void)
{
  std::for_each(_mod.begin(), _mod.end(),
                [this](std::pair<const std::string, Mod *> &mod){
                  this->unload(std::get<0>(mod));
                });
}

void            fyph::ModLoader::loadDir(const std::string &dir)
{
  char          *dirpnam;
  DIR           *dirp;
  struct dirent *ent;

  if (!(dirpnam = realpath(dir.c_str(), NULL)))
    throw (fyph::ModLoader::DirError(dir, "", _logger));
  if (!(dirp = opendir(dirpnam)))
	  throw (fyph::ModLoader::DirError(dir, "", _logger));
  while ((ent = readdir(dirp)))
  {
    std::string entname(ent->d_name);
    if (entname != "." && entname != "..")
    {
      try { load(std::string(dirpnam) + "/" + entname);
      } catch (const fyph::ModLoader::LoadError &e) { }
    }
  }
  free(dirpnam);
  closedir(dirp);
}

std::string		  fyph::ModLoader::dlError(void)
{
  size_t        pos;
  std::string   msg, err = "";
  char          *errmsg;

  if (!(errmsg = dlerror()))
    return (msg);
  err = errmsg;
  if ((pos = err.find(':')) != std::string::npos && pos < err.size() - 2)
    msg = err.substr(pos + 2);
  return (msg);
}

void             fyph::ModLoader::init(const std::string &fname)
{
  fyph::Mod      *mod;

  void *(*init)(void) =
    reinterpret_cast<void *(*)(void)>(dlsym(_handle[fname], "init"));
  if (!init)
    throw (fyph::ModLoader::LoadError(fname, "initialization failed", _logger));
  if (!(mod = (fyph::Mod *)init()))
    throw (fyph::ModLoader::LoadError(fname, "initialization failed", _logger));
  if (_logger)
    _logger->notice("ModLoader", "module %s successfully loaded", fname.c_str());
  _mod[fname] = mod;
}

void             fyph::ModLoader::load(const std::string &pname)
{
  void           *handle;

  errno = 0;
  if (pname.size() < 1)
    throw (fyph::ModLoader::LoadError(pname, "invalid module pathname", _logger));
  std::string fname = pname.substr(pname.find_last_of("/\\") + 1);
  if (_mod.find(fname) != _mod.end())
    throw (fyph::ModLoader::LoadError(fname, "module already loaded", _logger));
  if ((handle = dlopen(pname.c_str(), RTLD_LAZY)))
  {
    _handle[fname] = handle;
    init(fname);
  }
  else
    throw (fyph::ModLoader::LoadError(fname, dlError(), _logger));
}

void               fyph::ModLoader::unload(const std::string &fname)
{
  errno = 0;
  if (_mod.find(fname) == _mod.end())
    throw (fyph::ModLoader::UnloadError(fname, "module not loaded", _logger));
  std::string name = fname;
  _mod[fname]->del();
  int err = dlclose(_handle[fname]);
  if (!err && _logger)
    _logger->notice("ModLoader", "module %s unloaded", fname.c_str());
  if (err)
    throw (fyph::ModLoader::UnloadError(name.c_str(), "dlclose", _logger));
}

fyph::Verdict      fyph::ModLoader::returnPkt(Pkt *pkt, t_iovec *pdata)
{
  fyph::Verdict verdict = pkt->verdict;
  pkt->mangle();
  t_iovec *t = pkt->get();
  pdata->iov_base = t->iov_base;
  pdata->iov_len = t->iov_len;
  delete (pkt);
  return (verdict);
}

fyph::Verdict      fyph::ModLoader::callMods(t_iovec *pdata, uint32_t hwproto,
                                             int hook, const uint8_t hw[8])
{
  try {
    fyph::Pkt    *pkt = new fyph::Pkt(0, htons(hwproto), pdata);
    pkt->verdict = fyph::Verdict::ACCEPT;
    std::for_each(_mod.begin(), _mod.end(),
      [this, pkt, hook, hw](std::pair<std::string, Mod *> mod) {
        if (pkt->verdict == fyph::Verdict::ACCEPT)
           pkt->verdict = std::get<1>(mod)->callback(this->data, pkt, hook, hw);
              });
    if (pkt->verdict == fyph::Verdict::ACCEPT)
        return (returnPkt(pkt, pdata));
  } catch (const std::runtime_error &e) {
    std::cout << "[-] Ill formed packet: " << e.what() << std::endl;
  }
  return (fyph::Verdict::DROP);
}


std::string         fyph::ModLoader::Error::getMsg(const std::string &act,
                                                 const std::string &msg)
{
  const std::string &err = fyph::ModLoader::dlError();

  if (err != "")
    return (act + ": " + msg + ": " + err);
  return ("ModLoader: " + act + ": " + msg);
}
