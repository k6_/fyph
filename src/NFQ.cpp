/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <NFQ.hpp>

fyph::NFQ::NFQ(int queue, fyph::Logger *logger)
  : fyph::NLSck(NETLINK_NETFILTER, logger), _queue(queue)
{
  uint8_t             *buf;
  struct nlmsghdr     *nlhdr;

  if (!(buf = (uint8_t *)malloc(sizeof(*buf) * _bufsz)))
    throw (std::runtime_error("Memory exhausted"));
  nlhdr = setHdr(buf, NFQNL_MSG_CONFIG, _queue);
  nfq_nlmsg_cfg_put_cmd(nlhdr, AF_INET, NFQNL_CFG_CMD_PF_UNBIND);
  if (mnl_socket_sendto(_hdl, nlhdr, nlhdr->nlmsg_len) < 0)
    throw (NFQ::Error("failed to unbind queue", _queue));
  nlhdr = setHdr(buf, NFQNL_MSG_CONFIG, _queue);
  nfq_nlmsg_cfg_put_cmd(nlhdr, AF_INET, NFQNL_CFG_CMD_PF_BIND);
  if (mnl_socket_sendto(_hdl, nlhdr, nlhdr->nlmsg_len) < 0)
    throw (NFQ::Error("failed to bind queue", _queue));
  free(buf);
  if (_logger)
    _logger->notice(std::string("NFQ"), std::string("watching queue %d"), queue);
}

fyph::NFQ::~NFQ(void)
{
  if (_qHandle)
    nfq_destroy_queue(_qHandle);
  if (_handle)
    nfq_close(_handle);
}

struct nlmsghdr      *fyph::NFQ::setHdr(uint8_t *buf, int type, uint32_t queue)
{
  struct nlmsghdr    *nlhdr;
  struct nfgenmsg    *nfg;

  nlhdr = mnl_nlmsg_put_header(buf);
  nlhdr->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | type;
  nlhdr->nlmsg_flags = NLM_F_REQUEST;
  nfg = (struct nfgenmsg *)mnl_nlmsg_put_extra_header(nlhdr, sizeof(*nfg));
  nfg->nfgen_family = AF_UNSPEC;
  nfg->version = NFNETLINK_V0;
  nfg->res_id = htons(queue);
  return (nlhdr);
}

t_iovec               *fyph::NFQ::getPkt(struct nfq_data *nfa, int hook)
{
  t_iovec             *pdata;

  if (!(pdata = (struct iovec *)malloc(sizeof(*pdata))))
    throw (std::runtime_error("memory exhausted"));
  pdata->iov_base = NULL;
  if (!(pdata->iov_len = nfq_get_payload(nfa, (unsigned char **)&pdata->iov_base)))
    throw (NFQ::Error("cannot get packet's payload", hook));
  return (pdata);
}

void                  fyph::NFQ::treatPkt(int queue, struct nlattr **attr,
                                         struct nfqnl_msg_packet_hdr *ph,
                                         fyph::ModLoader *loader)
{
  int                 id;
  int                 verdict;
  struct iovec        *pdata;
  struct nfqnl_msg_packet_hw   *hw = NULL;

  id = ntohl(ph->packet_id);
  std::cout << "packet received(id=" << id << " hw=" << std::hex
    << ntohs(ph->hw_protocol) << " hook=" << std::dec <<
    (uint32_t)ph->hook << std::endl;
  if (!(pdata = (struct iovec *)malloc(sizeof(*pdata))))
    throw (std::runtime_error("memory exhausted"));
  pdata->iov_len = mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]);
  pdata->iov_base = mnl_attr_get_payload(attr[NFQA_PAYLOAD]);

  std::cout << "UPSTREAM" << std::endl << pdata << std::endl;
  verdict = loader->callMods(pdata, ph->hw_protocol, ph->hook,
                                 hw ? hw->hw_addr : NULL);
  std::cout << "DOWNSTREAM" << std::endl << pdata << std::endl;
  verdict = verdict == fyph::Verdict::ACCEPT ? NF_ACCEPT : NF_DROP;
  setVerdict(loader->data->queue[queue]->getSck(), queue, id, verdict,
             pdata, &loader->data->logger);
  free(pdata);
}

void                 fyph::NFQ::setVerdict(struct mnl_socket *hdl, uint32_t queue,
                                           uint32_t id, uint32_t verdict,
                                           struct iovec *pdata,
                                           fyph::Logger *logger)
{
  uint32_t           *buf = NULL;
  struct             nlmsghdr *nlhdr;

  if (!(buf = (uint32_t *)malloc(sizeof(*buf) * MNL_SOCKET_BUFFER_SIZE)))
    throw (std::runtime_error("memory exhausted"));
  nlhdr = setHdr((uint8_t *)buf, NFQNL_MSG_VERDICT, queue);
  nfq_nlmsg_verdict_put_pkt(nlhdr, pdata->iov_base, pdata->iov_len);
  nfq_nlmsg_verdict_put(nlhdr, id, verdict);
  if (mnl_socket_sendto(hdl, nlhdr, nlhdr->nlmsg_len) < 0)
    throw (NFQ::Error("failed to send verdict", queue, logger));
}

int                  fyph::NFQ::callback(const struct nlmsghdr *nlhdr,
                                         void *data)
{
  struct nlattr      *attr[NFQA_MAX+1];
  struct nfgenmsg    *nfg;
  uint32_t           queue;
  struct ModLoader   *loader;
  struct nfqnl_msg_packet_hdr *ph = NULL;

  loader = (ModLoader *)data;
  if (nfq_nlmsg_parse(nlhdr, attr) < 0)
    throw (NFQ::Error("failed to parse packet's attributes", -1,
                      &loader->data->logger));
  nfg = (struct nfgenmsg *)mnl_nlmsg_get_payload(nlhdr);
  queue = ntohs(nfg->res_id);
  if (!(ph = (struct nfqnl_msg_packet_hdr *)
    mnl_attr_get_payload(attr[NFQA_PACKET_HDR])))
    throw (NFQ::Error((const char *)"failed to parse packet's metaheader",
                      queue, &loader->data->logger));
  treatPkt(queue, attr, ph, loader);
  return (0);
}

void                 fyph::NFQ::setOptions(bool (*live)(void), unsigned timeout)
{
  uint8_t             *buf;
  struct nlmsghdr     *nlhdr;

  if (!(buf = (uint8_t *)malloc(sizeof(*buf) * _bufsz)))
    throw (std::runtime_error("Memory exhausted"));
  _live = live;
  struct timeval tv = {0, timeout};
  setsockopt(_fd, SOL_SOCKET, SO_RCVTIMEO,  (const char*)&tv,
             sizeof(struct timeval));
  nlhdr = setHdr(buf, NFQNL_MSG_CONFIG, _queue);
  nfq_nlmsg_cfg_put_cmd(nlhdr, AF_INET, NFQNL_CFG_CMD_BIND);
  if (mnl_socket_sendto(_hdl, nlhdr, nlhdr->nlmsg_len) < 0)
    throw (NFQ::Error((const char *)"failed to bind queue", _queue, _logger));
  nlhdr = setHdr(buf, NFQNL_MSG_CONFIG, _queue);
  nfq_nlmsg_cfg_put_params(nlhdr, NFQNL_COPY_PACKET, 0xffff);
  if (mnl_socket_sendto(_hdl, nlhdr, nlhdr->nlmsg_len) < 0)
    throw (NFQ::Error((const char *)"failed set options", _queue, _logger));
  free(buf);
}

ssize_t              fyph::NFQ::recvPkt(t_iovec *pkt)
{
  ssize_t            rvsz = 0;

  while (rvsz == 0)
  {
    errno = 0;
    rvsz = mnl_socket_recvfrom(_hdl, pkt->iov_base, pkt->iov_len);
    pkt->iov_len = rvsz;
  }
  return (rvsz);
}

void                 fyph::NFQ::intercept(fyph::NFQ *inst, NFQ::Error *err,
                                          void *data, fyph::Logger *logger)
{
  struct iovec       pkt;

  pkt.iov_base = NULL;
  while (!err)
  {
    if (!pkt.iov_base && !(pkt.iov_base = malloc(MNL_SOCKET_BUFFER_SIZE)))
      throw (std::runtime_error("memory exhausted"));
    pkt.iov_len = MNL_SOCKET_BUFFER_SIZE;
    if (inst->recvPkt(&pkt) < 1 && errno != EAGAIN)
      err =  new NFQ::Error((const char *)"reading from queue", inst->_queue,
                            logger);
    if (inst->_live && !inst->_live())
      break ;
    try {
      if (!err && !errno)
        mnl_cb_run(pkt.iov_base, pkt.iov_len, 0, inst->getPort(),
                   &fyph::NFQ::callback, data);
    } catch (const NFQ::Error &e) {
      if (logger)
        logger->error("NFQ", e.what());
    }
    free (pkt.iov_base);
    pkt.iov_base = NULL;
  }
  free (pkt.iov_base);
  logger->notice("NFQ", "stop watching queue %d", inst->_queue);
}

fyph::NFQ::Error::Error(const std::string &msg, int queue, fyph::Logger *logger)
  : fyph::Error("NFQ", logger, "queue %d: %s", queue, msg.c_str())
{  }

fyph::NFQ::Error::Error(const char *msg, int queue, fyph::Logger *logger)
  : fyph::Error("NFQ", logger, "queue %d: %s", queue, msg)
{  }
