/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <signal.h>
#include <stdlib.h>

#include <list>

#include <Fyph.hpp>

static int        usage(const char *pname)
{
  std::cout << "usage: " << pname
    << "[ -m mod.so... ] [ -d mod_dir... ] -q queue_id... -k 000aes-128-key00"
    << std::endl;
  return (1);
}

static void       term_hdl(int)
{
  fyph::Fyph::kill();
}

static int        cmd_args(int argc, char **argv, fyph::Fyph &fyph, t_iovec *key)
{
  int             opt;
  std::list<int> queue;
  std::list<std::string> mod;
  std::list<std::string> modDir;

  while ((opt = getopt(argc, argv, "k:m:q:d:")) != EOF)
  {
    switch (opt)
    {
      case 'm':
        mod.push_back(std::string(optarg));
        break ;
      case 'd':
        modDir.push_back(std::string(optarg));
        break ;
      case 'q':
        queue.push_back(strtol(optarg, NULL, 10));
        break ;
      case 'k':
        key->iov_base = optarg;
        break ;
      case '?':
        return (-1);
      default:
        return (-1);
    }
  }
  if (!key->iov_base || queue.size() < 1)
    return (-1);
  for (auto it = mod.begin(); it != mod.end(); it++)
  {
    try {
      fyph.addMod(*it);
    } catch (const fyph::ModLoader::Error &e) {
      continue ;
    }
  }
  for (auto it = modDir.begin(); it != modDir.end(); it++)
  {
    try {
      fyph.addModDir(*it);
    } catch (const fyph::ModLoader::Error &e) {
      continue ;
    }
  }
  for (auto it = queue.begin(); it != queue.end(); it++)
  {
    try {
      fyph.addQueue(*it);
    } catch (const fyph::NFQ::Error &e) {
      continue ;
    }
  }
  return (0);
}

int               main(int argc, char **argv)
{
  struct iovec    key;

  key.iov_base = NULL;
  key.iov_len = 16;
  fyph::Fyph    fyph(&key);
  signal(SIGINT, &term_hdl);
  if (cmd_args(argc, argv, fyph, &key))
    return (usage(argv[0]));
  fyph.run();
  return (0);
}
