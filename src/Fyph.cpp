/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <Fyph.hpp>

std::atomic<bool> alive(true);

bool              fyph::Fyph::live(void)
{
  return (alive.load());
}

void              fyph::Fyph::kill(void)
{
  std::cout << "killed" << std::endl;
  alive.store(false);
}

fyph::Fyph::Fyph(struct iovec *iov)
  : _mtx(new std::mutex())
{
  _logger = new Logger();
  _loader = new ModLoader(_logger);
  _loader->data = new fyph::ModData(iov, *_logger, _queue);
}

fyph::Fyph::~Fyph(void)
{
  std::for_each(_queue.begin(), _queue.end(),
                [this](const std::pair<int, fyph::NFQ *> &p) {
                 delete (std::get<1>(p));
                 delete (_thread[std::get<0>(p)]);
                });
  _queue.clear();
  _thread.clear();
  delete (_loader->data);
  delete (_loader);
  delete (_logger);
  delete (_mtx);
}

void              fyph::Fyph::addQueue(int queue)
{
  if (_queue.find(queue) != _queue.end())
    throw (fyph::NFQ::Error("queue is already watched", queue));
  fyph::NFQ *nfq = new fyph::NFQ(queue, _logger);
  _queue[queue] = nfq;
  try {
    _queue[queue]->setOptions(&fyph::Fyph::live);
  } catch (const NFQ::Error &e) {
    rmQueue(queue);
  }
}

void              fyph::Fyph::rmQueue(int queue)
{
  if (_queue.find(queue) == _queue.end())
    throw (fyph::NFQ::Error("queue is not watched", queue));
  delete (_thread[queue]);
  delete (_queue[queue]);
  _thread.erase(queue);
  _queue.erase(queue);
}

void              fyph::Fyph::watchQueue(int queue)
{
  NFQ::Error      *err = NULL;

  _thread[queue] = new std::thread(&fyph::NFQ::intercept, _queue[queue], err,
                                   _loader, _logger);
  if (err)
    throw (err);
}

void               fyph::Fyph::run(void)
{
  std::for_each(_queue.begin(), _queue.end(),
                [this](const std::pair<int, fyph::NFQ *> &p) {
                  try {
                    this->watchQueue(std::get<0>(p));
                  } catch (const NFQ::Error &e) {
                    rmQueue(std::get<0>(p));
                  }
                });
  std::for_each(_thread.begin(), _thread.end(),
                [this](const std::pair<int, std::thread *> &p) {
                  std::get<1>(p)->join();
                });
}

void            fyph::Fyph::addMod(const std::string &mod)
{
  _loader->load(mod);
}

void            fyph::Fyph::addModDir(const std::string &modDir)
{
  _loader->loadDir(modDir);
}
