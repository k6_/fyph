/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/
#include <NLSck.hpp>

//
#include <exception>
//

fyph::NLSck::NLSck(int type, fyph::Logger *log)
  : _logger(log), _bufsz(MNL_SOCKET_BUFFER_SIZE)
{
  if (!(_hdl = mnl_socket_open(type)))
    throw (std::runtime_error("change that"));
  if (mnl_socket_bind(_hdl, 0, MNL_SOCKET_AUTOPID) < 0)
    throw (std::runtime_error("change that"));
  _fd = mnl_socket_get_fd(_hdl);
  _port = mnl_socket_get_portid(_hdl);
}
