/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

# include <GCipher.hpp>

void             *init(void)
{
  /* Init gcrypt and check version */
  if (!gcry_check_version(GCRYPT_VERSION))
    throw (std::exception());
  /* Disable secure memory */
  gcry_control(GCRYCTL_DISABLE_SECMEM, 0);
  /* Initialization completed */
  gcry_control(GCRYCTL_INITIALIZATION_FINISHED, 0);
  fyph::GCipher *m = new fyph::GCipher();
  return (m);
}

void                fyph::GCipher::del(void)
{
  delete (this);
}

fyph::GCipher::Cipher::Cipher(int cipher, int mode)
  : _cipher(cipher), _mode(mode), _key(NULL)
{
  gcry_cipher_open(&_handle, _cipher, _mode, 0);
}


fyph::GCipher::Cipher::~Cipher(void)
{
  gcry_cipher_close(_handle);
}

/*
 * Need secure key retrieval
 */
void                fyph::GCipher::Cipher::setKey(const t_iovec *key)
{
  _key = key;
  if (gcry_cipher_get_algo_keylen(_cipher) != key->iov_len)
    throw (std::runtime_error("invalid key length"));
  gcry_cipher_setkey(_handle, _key->iov_base, _key->iov_len);
}

t_iovec             *fyph::GCipher::Cipher::align(t_iovec *vec, size_t blksize,
                                                  bool always)
{
  t_iovec           *aligned;
  size_t            padsz;

  aligned = vec;
  if (always)
  padsz = vec->iov_len % blksize ? blksize - (vec->iov_len % blksize)
    : blksize;
  else
    padsz = vec->iov_len % blksize;
  if (vec->iov_len + padsz > IP_MAXPACKET)
    throw (std::runtime_error("packet too large"));
  for (size_t i = 0; i < padsz; i++)
    *(uint8_t *)((uint64_t)vec->iov_base + vec->iov_len + i) = (uint8_t)padsz;
  vec->iov_len += padsz;
  return (aligned);
}

/*
 * Need automated allocation for encrypted data buffer, and thus to calculate
 * the necessary space and padding for each algorithm implemented / not just AES128.
 */
void                fyph::GCipher::Cipher::encrypt(t_iovec *clrVec,
                                                   t_iovec *encVec)
{
  t_iovec           *aligned;

  aligned = align(clrVec, 16, true);
  if (!(encVec->iov_base = malloc(aligned->iov_len)))
    throw (std::runtime_error("memory exhausted"));
  encVec->iov_len = aligned->iov_len;
  int err = gcry_cipher_encrypt(_handle, encVec->iov_base, encVec->iov_len,
            aligned->iov_base, aligned->iov_len);
  if (err)
    std::cout << gcry_strsource(err) << ": " << gcry_strerror(err) << std::endl;
}

void                fyph::GCipher::Cipher::decrypt(t_iovec *clrVec,
                                                   t_iovec *encVec)
{
  t_iovec           *aligned;
 
  aligned = align(encVec, 16);
  if (!(clrVec->iov_base = malloc(aligned->iov_len)))
     throw (std::runtime_error("memory exhausted"));
  clrVec->iov_len = aligned->iov_len;
  int err = gcry_cipher_decrypt(_handle, clrVec->iov_base, clrVec->iov_len,
             aligned->iov_base, aligned->iov_len);
  if (err)
    std::cout << gcry_strsource(err) << ": " << gcry_strerror(err) << std::endl;
  else
  clrVec->iov_len -= *(uint8_t *)((uint64_t)clrVec->iov_base + clrVec->iov_len - 1);
}

void                fyph::GCipher::Cipher::eCall(t_iovec *pdata)
{
  t_iovec           encVec;

  std::cout << "->-- CLRTXT [" << pdata->iov_len << "] ->--" << std::endl;
  std::cout << pdata << std::endl;
  encrypt(pdata, &encVec);
  std::cout << "->-- CPHTXT ->--" << std::endl;
  std::cout << &encVec << std::endl;
  pdata->iov_base = encVec.iov_base;
  pdata->iov_len = encVec.iov_len;
}

void                fyph::GCipher::Cipher::dCall(t_iovec *pdata)
{
  t_iovec           clrVec;

  std::cout << "-<-- CPHTXT [" << pdata->iov_len << "] --<-" << std::endl;
  std::cout << pdata << std::endl;
  decrypt(&clrVec, pdata);
  std::cout << "-<-- CLRTXT --<-" << std::endl;
  std::cout << &clrVec << std::endl;
  pdata->iov_base = clrVec.iov_base;
  pdata->iov_len = clrVec.iov_len;
}

struct          tcpPseudoHeader
{
  uint32_t      saddr;
  uint32_t      daddr;
  uint8_t       pad;
  uint8_t       proto;
  uint16_t      sz;
} __attribute__((packed));

fyph::Verdict       fyph::GCipher::Cipher::treatPkt(fyph::ModData *mData,
                                                    fyph::Pkt *pkt, int hook)
{
  fyph::TCP         *tcp;

  if ((tcp = pkt->layer<fyph::TCP>()) && tcp->getID() == IPPROTO_TCP)
  {
    struct iovec *data = tcp->getData();
    setKey(mData->key);
    if (data->iov_len > 0)
    {
      if (data->iov_len > 0 && hook == 1)
        dCall(data);
      else if (data->iov_len > 0)
        eCall(data);
    }
  }
  return (fyph::Verdict::ACCEPT);
}

fyph::Verdict       fyph::GCipher::callback(void *data, void *pkt, int hook,
                                            const uint8_t *)
{
  fyph::IP          *ip;
  fyph::ModData     *mData;

  try {
    if ((ip = ((fyph::Pkt *)pkt)->layer<fyph::IP>())
        && ip->getID() == ETHERTYPE_IP)
    {
      mData = (fyph::ModData *)data;
      fyph::GCipher::Cipher    ciph
            = fyph::GCipher::Cipher(GCRY_CIPHER_AES128, GCRY_CIPHER_MODE_ECB);
      return (ciph.treatPkt(mData, (fyph::Pkt *)pkt, hook));
    }
  } catch (std::runtime_error e) {
    return (fyph::Verdict::DROP);
  }
  return (fyph::Verdict::ACCEPT);
}
