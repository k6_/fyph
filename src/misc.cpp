/*---+                                FYPH                               +---*\
#               __Packet Analysis, Filtering and Tampering__                  #
#                         _Part of the IoTSec Suite_                          #
#                              _Epitech EIP PoC_                              #
#                                                                             #
\*-+                                                                       +-*/

#include <misc.hpp>

void       fyph::dumpHex(std::ostream &out, const uint8_t *dat, size_t datsz)
{
  for (size_t j = 0; j < datsz; j++)
  {
    if (j && !(j % 4))
      out << " ";
    out << std::hex << std::setw(2) << std::setfill('0') << (dat[j] & 0xff);
    out << " ";
  }
}

void       fyph::dumpAscii(std::ostream &out, const uint8_t *dat, size_t datsz)
{
  for (size_t j = 0; j < datsz; j++)
  {
		if (isprint(*((uint8_t *)dat + j)))
        out << *((uint8_t *)dat + j);
    else
      out << '.';
  }
}
