
---
# FYPH
__Packet Analysis, Filtering and Tampering__
_Part of the IoTSec Suite_
_Epitech EIP PoC_
---


## PURPOSE

  This software's purpose is to provide a modular interface for packet's
analysis, filetering and tampering. All modules must be placed in a directory,
and must be standard .so shared libraries.

## DEPENDENCIES

  - libnetfiler_queue
  - nftables or iptables

## LIBNETFILTER_QUEUE

  _"is a userspace library providing an API to packets that have been queued
by the kernel packet filter."_ This software rely on it for packet's
interception and filtering. Refer to [libfilter project's documentation](http://www.netfilter.org/projects/libnetfilter_queue/)
for details.
  Is it necessary to keep in mind that using this library, we do not get the
ethernet header - or any else layer 2 header. It might be possible using nftables
 with an ingress rule.

## COMPILE

  Just make it.

## RUN

  First, the desired traffic must be queued with iptables.

    # iptables -A INPUT -j NFQUEUE --queue-num 0
    # iptables -A OUTPUT -j NFQUEUE --queue-num 1

Then just run fyph:

    # fyph -k keykeykeykeykeyy [ -m ./mod.so ] [ -d ./modDir/ ] -q 0 -q 1

It should start dumping the packets it receives.

## MODULES

  Modules can be loaded via command-line arguments. They must implement the
fyph::Mod interface. Just compile them as you would for any standard linux .so.

		$ g++ -c -fPIC myMod.cpp -std=c++14
		$ g++ -shared myMod.o -o mods/myMod.so

### GCipher

  GCipher is used to encrypt output and encrypt input traffic. It currently
supports AES-128 and the same key is used for every clients.
  Please note that we encrypt all the output traffic, and try to decrypt
everything we receive. You will want to setup __smarter iptables rules__ so you
can still speak to the rest of the world. See the queue_port.sh to filter by
port.

## PROJECT STRUCTURE

  Please refer to the [XML class diagram](doc/xml_class.pdf) and any other
resource which might be present below the doc/ directory.
